import 'package:adoptar_app/modules/core/presentation/widgets/app_bar_widget.dart';
import 'package:adoptar_app/modules/posts/domain/post_type_entity.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:adoptar_app/modules/core/routes/router.gr.dart' as app_router;

class MainCreatePost extends StatelessWidget {
  MainCreatePost({Key key}) : super(key: key);

  static Route<dynamic> route() => MaterialPageRoute(
        builder: (context) => MainCreatePost(),
      );

  final List<PostTypeEntity> _postTypes = [
    new PostTypeEntity(
      title: "Mascota en adopcion",
      path: app_router.Routes.create_post,
    ),
    new PostTypeEntity(
      title: "Mascota perdido",
      path: app_router.Routes.create_post,
    ),
    new PostTypeEntity(
      title: "Mascota encontrada",
      path: app_router.Routes.create_post,
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: "Crear adopción",
        appBar: AppBar(),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Text("¿Qué queres publicar?"),
              ListView.builder(
                shrinkWrap: true,
                itemCount: _postTypes.length,
                itemBuilder: (context, index) {
                  final PostTypeEntity postType = _postTypes[index];
                  return GestureDetector(
                    child: Container(
                      height: 100,
                      width: MediaQuery.of(context).size.width,
                      child: Card(
                        child: Text(postType.title),
                      ),
                    ),
                    onTap: () {
                      ExtendedNavigator.of(context).push(
                        postType.path,
                        arguments: app_router.CreatePostPageArguments(
                          appbarTitle: postType.title,
                        ),
                      );
                    },
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
