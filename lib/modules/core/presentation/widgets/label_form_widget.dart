import 'package:flutter/material.dart';

class LabelForm extends StatelessWidget {
  const LabelForm({Key key, this.label = ""})
      : assert(label != null, 'label cannot be null.'),
        super(key: key);

  final String label;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: 8),
      child: Text(
        this.label,
        style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
      ),
    );
  }
}
