import 'package:adoptar_app/modules/posts/domain/post_entity.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class PostPage extends StatelessWidget {
  final PostEntity postEntity;

  const PostPage({Key key, @required this.postEntity}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            forceElevated: true,
            flexibleSpace: FlexibleSpaceBar(
              centerTitle: true,
              background: Swiper(
                pagination: const SwiperPagination(),
                itemCount: postEntity.imageUrls.length,
                itemBuilder: (BuildContext context, int index) => Image.network(
                  postEntity.imageUrls[index],
                  fit: BoxFit.cover,
                ),
                autoplay: true,
              ),
            ),
            expandedHeight: 300,
            leading: new IconButton(
              icon: new Icon(Icons.arrow_back),
              onPressed: () {
                ExtendedNavigator.of(context).pop();
              },
            ),
          ),
          SliverList(
            delegate: SliverChildListDelegate(
              [
                Container(
                  margin: EdgeInsets.symmetric(
                    horizontal: 16,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ListTile(
                        leading: CircleAvatar(
                          radius: 20.0,
                          backgroundImage:
                              NetworkImage(postEntity.createdBy.photoURL),
                          backgroundColor: Colors.transparent,
                          child: postEntity.createdBy.photoURL == ""
                              ? Text(postEntity.createdBy.displayName[0])
                              : null,
                        ),
                        title: Text(postEntity.createdBy.displayName),
                      ),
                      Text(postEntity.description),
                      Row(
                        children: [
                          Expanded(
                            child: Container(
                              height: 200,
                              child: Placeholder(),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 16, vertical: 24),
                  child: Text("lorem10"),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
