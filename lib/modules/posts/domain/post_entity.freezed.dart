// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'post_entity.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$PostEntityTearOff {
  const _$PostEntityTearOff();

// ignore: unused_element
  _PostEntity call(
      {@required UniqueId id,
      @required String title,
      @required String description,
      @required List<String> imageUrls,
      @required UserEntity createdBy}) {
    return _PostEntity(
      id: id,
      title: title,
      description: description,
      imageUrls: imageUrls,
      createdBy: createdBy,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $PostEntity = _$PostEntityTearOff();

/// @nodoc
mixin _$PostEntity {
  UniqueId get id;
  String get title;
  String get description;
  List<String> get imageUrls;
  UserEntity get createdBy;

  $PostEntityCopyWith<PostEntity> get copyWith;
}

/// @nodoc
abstract class $PostEntityCopyWith<$Res> {
  factory $PostEntityCopyWith(
          PostEntity value, $Res Function(PostEntity) then) =
      _$PostEntityCopyWithImpl<$Res>;
  $Res call(
      {UniqueId id,
      String title,
      String description,
      List<String> imageUrls,
      UserEntity createdBy});

  $UserEntityCopyWith<$Res> get createdBy;
}

/// @nodoc
class _$PostEntityCopyWithImpl<$Res> implements $PostEntityCopyWith<$Res> {
  _$PostEntityCopyWithImpl(this._value, this._then);

  final PostEntity _value;
  // ignore: unused_field
  final $Res Function(PostEntity) _then;

  @override
  $Res call({
    Object id = freezed,
    Object title = freezed,
    Object description = freezed,
    Object imageUrls = freezed,
    Object createdBy = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed ? _value.id : id as UniqueId,
      title: title == freezed ? _value.title : title as String,
      description:
          description == freezed ? _value.description : description as String,
      imageUrls:
          imageUrls == freezed ? _value.imageUrls : imageUrls as List<String>,
      createdBy:
          createdBy == freezed ? _value.createdBy : createdBy as UserEntity,
    ));
  }

  @override
  $UserEntityCopyWith<$Res> get createdBy {
    if (_value.createdBy == null) {
      return null;
    }
    return $UserEntityCopyWith<$Res>(_value.createdBy, (value) {
      return _then(_value.copyWith(createdBy: value));
    });
  }
}

/// @nodoc
abstract class _$PostEntityCopyWith<$Res> implements $PostEntityCopyWith<$Res> {
  factory _$PostEntityCopyWith(
          _PostEntity value, $Res Function(_PostEntity) then) =
      __$PostEntityCopyWithImpl<$Res>;
  @override
  $Res call(
      {UniqueId id,
      String title,
      String description,
      List<String> imageUrls,
      UserEntity createdBy});

  @override
  $UserEntityCopyWith<$Res> get createdBy;
}

/// @nodoc
class __$PostEntityCopyWithImpl<$Res> extends _$PostEntityCopyWithImpl<$Res>
    implements _$PostEntityCopyWith<$Res> {
  __$PostEntityCopyWithImpl(
      _PostEntity _value, $Res Function(_PostEntity) _then)
      : super(_value, (v) => _then(v as _PostEntity));

  @override
  _PostEntity get _value => super._value as _PostEntity;

  @override
  $Res call({
    Object id = freezed,
    Object title = freezed,
    Object description = freezed,
    Object imageUrls = freezed,
    Object createdBy = freezed,
  }) {
    return _then(_PostEntity(
      id: id == freezed ? _value.id : id as UniqueId,
      title: title == freezed ? _value.title : title as String,
      description:
          description == freezed ? _value.description : description as String,
      imageUrls:
          imageUrls == freezed ? _value.imageUrls : imageUrls as List<String>,
      createdBy:
          createdBy == freezed ? _value.createdBy : createdBy as UserEntity,
    ));
  }
}

/// @nodoc
class _$_PostEntity extends _PostEntity with DiagnosticableTreeMixin {
  const _$_PostEntity(
      {@required this.id,
      @required this.title,
      @required this.description,
      @required this.imageUrls,
      @required this.createdBy})
      : assert(id != null),
        assert(title != null),
        assert(description != null),
        assert(imageUrls != null),
        assert(createdBy != null),
        super._();

  @override
  final UniqueId id;
  @override
  final String title;
  @override
  final String description;
  @override
  final List<String> imageUrls;
  @override
  final UserEntity createdBy;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'PostEntity(id: $id, title: $title, description: $description, imageUrls: $imageUrls, createdBy: $createdBy)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'PostEntity'))
      ..add(DiagnosticsProperty('id', id))
      ..add(DiagnosticsProperty('title', title))
      ..add(DiagnosticsProperty('description', description))
      ..add(DiagnosticsProperty('imageUrls', imageUrls))
      ..add(DiagnosticsProperty('createdBy', createdBy));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _PostEntity &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.title, title) ||
                const DeepCollectionEquality().equals(other.title, title)) &&
            (identical(other.description, description) ||
                const DeepCollectionEquality()
                    .equals(other.description, description)) &&
            (identical(other.imageUrls, imageUrls) ||
                const DeepCollectionEquality()
                    .equals(other.imageUrls, imageUrls)) &&
            (identical(other.createdBy, createdBy) ||
                const DeepCollectionEquality()
                    .equals(other.createdBy, createdBy)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(title) ^
      const DeepCollectionEquality().hash(description) ^
      const DeepCollectionEquality().hash(imageUrls) ^
      const DeepCollectionEquality().hash(createdBy);

  @override
  _$PostEntityCopyWith<_PostEntity> get copyWith =>
      __$PostEntityCopyWithImpl<_PostEntity>(this, _$identity);
}

abstract class _PostEntity extends PostEntity {
  const _PostEntity._() : super._();
  const factory _PostEntity(
      {@required UniqueId id,
      @required String title,
      @required String description,
      @required List<String> imageUrls,
      @required UserEntity createdBy}) = _$_PostEntity;

  @override
  UniqueId get id;
  @override
  String get title;
  @override
  String get description;
  @override
  List<String> get imageUrls;
  @override
  UserEntity get createdBy;
  @override
  _$PostEntityCopyWith<_PostEntity> get copyWith;
}
