import 'package:adoptar_app/modules/posts/presentation/widgets/create_post_widget.dart';
import 'package:flutter/material.dart';
import 'package:auto_route/auto_route.dart';

class CreatePostPage extends StatelessWidget {
  final String appbarTitle;

  const CreatePostPage({Key key, @required this.appbarTitle})
      : assert(appbarTitle != null, "Appbar title cannot be null"),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: CreatePostWidget(
        appbarTitle: this.appbarTitle,
      )
    );
  }
}
