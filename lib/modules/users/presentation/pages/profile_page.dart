import 'dart:math';

import 'package:adoptar_app/modules/core/presentation/widgets/app_bar_widget.dart';
import 'package:flutter/material.dart';

class ProfilePage extends StatelessWidget {
  static Route<dynamic> route() => MaterialPageRoute(
        builder: (context) => ProfilePage(),
      );

  final List<int> _selectedItems = List<int>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: "Profile",
        appBar: AppBar(),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Row(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(20),
                  child: Stack(
                    children: <Widget>[
                      CircleAvatar(
                        radius: 50,
                        backgroundImage: NetworkImage(
                            'https://spoiler.bolavip.com/__export/1596120429620/sites/bolavip/img/2020/07/30/ester_exposito_video_viral_instagram_elite_serie_netflix_crop1596120427160.jpeg_1693159006.jpeg'),
                      ), //Scaffold.of(context).showSnackBar(new SnackBar(content: new Text('edit avatar')));
                      Positioned(
                        bottom: 25 - sin(45) * 25,
                        right: 25 - sin(45) * 25,
                        child: DecoratedBox(
                          decoration: BoxDecoration(
                            color: Colors.brown[400],
                            borderRadius: BorderRadius.circular(16),
                          ),
                          child: Padding(
                            padding: EdgeInsets.all(6),
                            child: Icon(
                              Icons.edit,
                              size: 10,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(bottom: 10, right: 20),
                      child: Text(
                        'OCNYang · 歐心',
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Text(
                      'Flutter dev',
                      maxLines: 1,
                      style: TextStyle(
                        color: Colors.grey,
                        fontSize: 12,
                      ),
                    ),
                  ],
                )
              ],
            ),
            Container(
              padding: EdgeInsets.only(top: 10, bottom: 10),
              margin: EdgeInsets.only(left: 20, right: 20, bottom: 5),
              child: Row(
                children: <Widget>[
                  buildMapItemView('200', 'Recipes'),
                  buildMapItemView('5480', 'Views'),
                  buildMapItemView('1.3 k', 'Followers'),
                ],
              ),
            ),
            InkWell(
              onTap: () {},
              splashColor: Colors.black12,
              child: Padding(
                padding:
                    EdgeInsets.only(left: 20, right: 20, top: 15, bottom: 15),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Column(
                        children: <Widget>[
                          Text(
                            "Wallet",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 18,
                            ),
                          ),
                          Divider(
                            height: 5,
                            color: Colors.transparent,
                          ),
                          Text(
                            'Balance:\$ 254',
                            style: TextStyle(color: Colors.grey, fontSize: 12),
                          )
                        ],
                        crossAxisAlignment: CrossAxisAlignment.start,
                      ),
                      flex: 1,
                    ),
                    DecoratedBox(
                      decoration: BoxDecoration(
                          color: Colors.brown,
                          borderRadius: BorderRadius.all(Radius.circular(3))),
                      child: Padding(
                        padding: EdgeInsets.only(
                            left: 10, right: 10, top: 3, bottom: 3),
                        child: Text(
                          'Recharge',
                          style: TextStyle(color: Colors.white, fontSize: 12),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Expanded buildMapItemView(String value, String key) {
    return Expanded(
      child: Column(
        children: <Widget>[
          Text(
            value,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 18,
            ),
          ),
          Divider(
            height: 5,
            color: Colors.transparent,
          ),
          Text(
            key,
            style: TextStyle(color: Colors.grey, fontSize: 12),
          )
        ],
      ),
      flex: 1,
    );
  }
}
