import 'package:adoptar_app/modules/core/domain/unique_id.dart';
import 'package:adoptar_app/modules/posts/domain/post_entity.dart';
import 'package:adoptar_app/modules/posts/presentation/pages/post_page.dart';
import 'package:adoptar_app/modules/posts/presentation/widgets/post_item_widget.dart';
import 'package:adoptar_app/modules/users/domain/user_entity.dart';
import 'package:flutter/material.dart';
import 'package:adoptar_app/modules/core/routes/router.gr.dart' as app_router;
import 'package:auto_route/auto_route.dart';

class PostList extends StatelessWidget {
  PostList({Key key}) : super(key: key);

  final List<PostEntity> posts = List<PostEntity>.generate(
    20,
    (i) => PostEntity(
      title: 'Title $i',
      description: 'Description $i',
      id: new UniqueId(),
      imageUrls: [
        "https://perros.mascotahogar.com/Imagenes/raza-husky-siberiano.jpg",
        "https://www.hola.com/imagenes/estar-bien/20201005176621/perro-husky-raza-perro-lobo/0-874-110/husky-1-a.jpg"
      ],
      createdBy: new UserEntity(
        id: new UniqueId(),
        displayName: 'User name $i',
        photoURL: 'https://spoiler.bolavip.com/__export/1596120429620/sites/bolavip/img/2020/07/30/ester_exposito_video_viral_instagram_elite_serie_netflix_crop1596120427160.jpeg_1693159006.jpeg',
      ),
    ),
  );

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: posts.length,
      itemBuilder: (BuildContext context, int index) {
        return GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: () {
            ExtendedNavigator.of(context).push(
              app_router.Routes.post_page,
              arguments: app_router.PostPageArguments(postEntity: posts[index]),
            );
          },
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 0),
            child: PostItem(
              post: posts[index],
            ),
          ),
        );
      },
    );
  }
}
