import 'package:adoptar_app/modules/core/domain/unique_id.dart';
import 'package:adoptar_app/modules/users/domain/user_entity.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'post_entity.freezed.dart';

@freezed
abstract class PostEntity with _$PostEntity {
  const PostEntity._();

  const factory PostEntity({
    @required UniqueId id,
    @required String title,
    @required String description,
    @required List<String> imageUrls,
    @required UserEntity createdBy,
  }) = _PostEntity;
}
