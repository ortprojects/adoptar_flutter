import 'package:adoptar_app/modules/core/presentation/widgets/app_bar_widget.dart';
import 'package:adoptar_app/modules/core/presentation/widgets/label_form_widget.dart';
import 'package:flex_color_picker/flex_color_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tags/flutter_tags.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:auto_route/auto_route.dart';

class CreatePostWidget extends StatefulWidget {
  final String appbarTitle;

  const CreatePostWidget({Key key, @required this.appbarTitle})
      : assert(appbarTitle != null, "Appbar title cannot be null"),
        super(key: key);

  @override
  _CreatePostWidgetState createState() => _CreatePostWidgetState();
}

class _CreatePostWidgetState extends State<CreatePostWidget> {
  List<Asset> images = List<Asset>();
  String _error;
  List<ItemTags> _gender = [
    ItemTags(
      index: 1,
      title: "Macho",
      active: false,
    ),
    ItemTags(
      index: 2,
      title: "Hembra",
      active: false,
    )
  ];

  List<ItemTags> _age = [
    ItemTags(
      index: 1,
      title: "Cachorro",
      active: false,
    ),
    ItemTags(
      index: 2,
      title: "Joven",
      active: false,
    ),
    ItemTags(
      index: 3,
      title: "Adulto",
      active: false,
    )
  ];

  List<ItemTags> _size = [
    ItemTags(
      index: 1,
      title: "Pequeño",
      active: false,
    ),
    ItemTags(
      index: 2,
      title: "Mediano",
      active: false,
    ),
    ItemTags(
      index: 3,
      title: "Grande",
      active: false,
    )
  ];

  List<Color> pickerColors;

  // Define some custom colors for the custom picker segment.
  // The 'guide' color values are from
  // https://material.io/design/color/the-color-system.html#color-theme-creation
  static const Color guidePrimary = Color(0xFF6200EE);
  static const Color guidePrimaryVariant = Color(0xFF3700B3);
  static const Color guideSecondary = Color(0xFF03DAC6);
  static const Color guideSecondaryVariant = Color(0xFF018786);
  static const Color guideError = Color(0xFFB00020);
  static const Color guideErrorDark = Color(0xFFCF6679);
  static const Color blueBlues = Color(0xFF174378);

  // Make a custom ColorSwatch to name map from the above custom colors.
  final Map<ColorSwatch<Object>, String> colorsNameMap =
      <ColorSwatch<Object>, String>{
    ColorTools.createPrimarySwatch(guidePrimary): 'Guide Purple',
    ColorTools.createPrimarySwatch(guidePrimaryVariant): 'Guide Purple Variant',
    ColorTools.createAccentSwatch(guideSecondary): 'Guide Teal',
    ColorTools.createAccentSwatch(guideSecondaryVariant): 'Guide Teal Variant',
    ColorTools.createPrimarySwatch(guideError): 'Guide Error',
    ColorTools.createPrimarySwatch(guideErrorDark): 'Guide Error Dark',
    ColorTools.createPrimarySwatch(blueBlues): 'Blue blues',
  };

  @override
  void initState() {
    pickerColors = [Colors.blue, Colors.orange];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: "Crear publicacion",
        appBar: AppBar(),
        automaticallyImplyLeading: true,
      ),
      body: Scrollbar(
        child: SingleChildScrollView(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TextField(
                obscureText: false,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Nombre mascota',
                ),
              ),
              LabelForm(
                label: "Género",
              ),
              Tags(
                itemCount: _gender.length,
                columns: 2,
                itemBuilder: (int index) {
                  return ItemTags(
                    title: _gender[index].title,
                    index: index,
                    singleItem: true,
                    active: _gender[index].active,
                    onPressed: (item) => print(item),
                  );
                },
              ),
              LabelForm(
                label: "Edad",
              ),
              Tags(
                itemCount: _age.length,
                columns: 3,
                itemBuilder: (int index) {
                  return ItemTags(
                    title: _age[index].title,
                    index: index,
                    singleItem: true,
                    active: _age[index].active,
                    onPressed: (item) => print(item),
                  );
                },
              ),
              LabelForm(
                label: "Tamaño",
              ),
              Tags(
                itemCount: _size.length,
                columns: 3,
                itemBuilder: (int index) {
                  return ItemTags(
                    title: _size[index].title,
                    index: index,
                    singleItem: true,
                    active: _size[index].active,
                    onPressed: (item) => print(item),
                  );
                },
              ),
              LabelForm(
                label: "Colores de tu mascota",
              ),
              Row(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width / 2,
                    child: Column(
                      children: [
                        ColorIndicator(
                          width: MediaQuery.of(context).size.width,
                          height: 100,
                          borderRadius: 6,
                          color: pickerColors[0],
                          onSelect: () async {
                            final Color colorBeforeDialog = pickerColors[0];
                            if (!(await colorPickerDialog(0))) {
                              setState(() {
                                pickerColors[0] = colorBeforeDialog;
                              });
                            }
                          },
                        ),
                        Text(
                          '${ColorTools.materialNameAndCode(pickerColors[0], colorSwatchNameMap: colorsNameMap)} ',
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width / 2,
                    child: Column(
                      children: [
                        ColorIndicator(
                          width: MediaQuery.of(context).size.width,
                          height: 100,
                          borderRadius: 6,
                          color: pickerColors[1],
                          onSelect: () async {
                            final Color colorBeforeDialog = pickerColors[1];
                            if (!(await colorPickerDialog(1))) {
                              setState(() {
                                pickerColors[1] = colorBeforeDialog;
                              });
                            }
                          },
                        ),
                        Text(
                          '${ColorTools.materialNameAndCode(pickerColors[1], colorSwatchNameMap: colorsNameMap)} ',
                        )
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width,
                child: RaisedButton(
                  child: Text("Agrega fotos"),
                  onPressed: loadAssets,
                ),
              ),
              Container(
                height: 400,
                child: Expanded(
                  child: buildGridView(),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<bool> colorPickerDialog(index) async {
    return ColorPicker(
      color: pickerColors[index],
      onColorChanged: (Color color) =>
          setState(() => pickerColors[index] = color),
      width: 40,
      height: 40,
      borderRadius: 4,
      spacing: 5,
      runSpacing: 5,
      wheelDiameter: 155,
      heading: Text(
        'Selecciona un color',
        style: Theme.of(context).textTheme.subtitle1,
      ),
      subheading: Text(
        'Selecciona su sombra',
        style: Theme.of(context).textTheme.subtitle1,
      ),
      wheelSubheading: Text(
        'Color y sombra',
        style: Theme.of(context).textTheme.subtitle1,
      ),
      showMaterialName: true,
      showColorName: true,
      showColorCode: true,
      materialNameTextStyle: Theme.of(context).textTheme.caption,
      colorNameTextStyle: Theme.of(context).textTheme.caption,
      colorCodeTextStyle: Theme.of(context).textTheme.caption,
      pickerTypeLabels: const <ColorPickerType, String>{
        ColorPickerType.primary: "Primarios",
        ColorPickerType.wheel: "Rueda",
      },
      pickersEnabled: const <ColorPickerType, bool>{
        ColorPickerType.both: false,
        ColorPickerType.primary: true,
        ColorPickerType.accent: false,
        ColorPickerType.custom: false,
        ColorPickerType.wheel: true,
      },
      customColorSwatchesAndNames: colorsNameMap,
    ).showPickerDialog(
      context,
      cancelLabel: "Cancelar",
      selectLabel: "Seleccionar",
      constraints:
          const BoxConstraints(minHeight: 460, minWidth: 300, maxWidth: 320),
    );
  }

  Widget buildGridView() {
    if (images != null)
      return GridView.count(
        shrinkWrap: true,
        crossAxisCount: 3,
        children: List.generate(images.length, (index) {
          Asset asset = images[index];
          return AssetThumb(
            asset: asset,
            width: 300,
            height: 300,
          );
        }),
      );
    else
      return Container(color: Colors.white);
  }

  Future<void> loadAssets() async {
    setState(() {
      images = List<Asset>();
    });

    List<Asset> resultList;
    String error;

    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 300,
      );
    } on Exception catch (e) {
      error = e.toString();
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      images = resultList;
      if (error == null) _error = 'No Error Dectected';
    });
  }
}
