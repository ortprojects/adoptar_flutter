import 'package:auto_route/auto_route.dart';

class AuthGuard extends RouteGuard {
 @override
 Future<bool> canNavigate(
     ExtendedNavigatorState navigator, String routeName, Object arguments) async {

  //  SharedPreferences prefs = await SharedPreferences.getInstance();
  //  return prefs.getString('token_key') != null;
  return true;
 }
}