import 'dart:math';

import 'package:adoptar_app/modules/core/routes/router.gr.dart';
import 'package:adoptar_app/modules/posts/domain/post_entity.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

class PostItem extends StatelessWidget {
  final PostEntity post;

  const PostItem({
    Key key,
    @required this.post,
  }) : super(key: key);

  static const COLORS = [
    Color(0xFFEF7A85),
    Color(0xFFFF90B3),
    Color(0xFFFFC2E2),
    Color(0xFFB892FF),
    Color(0xFFB892FF)
  ];

  @override
  Widget build(BuildContext context) {
    return new Row(
      children: <Widget>[
        new Container(
            width: 10.0, height: 190.0, color: COLORS[new Random().nextInt(5)]),
        new Expanded(
          child: new Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 40.0, horizontal: 10.0),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Text(
                  post.title,
                  style: TextStyle(
                    color: Colors.grey.shade800,
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                new Padding(
                  padding: const EdgeInsets.only(top: 16.0),
                  child: new Text(
                    post.description,
                    style: TextStyle(
                      color: Colors.grey.shade500,
                      fontSize: 12.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        new Container(
          height: 150.0,
          width: 150.0,
          color: Colors.transparent,
          child: Stack(
            children: <Widget>[
              new Transform.translate(
                offset: new Offset(40.0, 0.0),
                child: new Container(
                  height: 100.0,
                  width: 200.0,
                  color: COLORS[new Random().nextInt(5)],
                ),
              ),
              new Transform.translate(
                offset: Offset(-10.0, 20.0),
                child: new Card(
                  elevation: 20.0,
                  child: new Container(
                    height: 120.0,
                    width: 220.0,
                    decoration: new BoxDecoration(
                        color: Colors.white,
                        border: Border.all(
                          width: 10.0,
                          color: Colors.white,
                          style: BorderStyle.solid,
                        ),
                        image: DecorationImage(
                          image: NetworkImage(
                            "https://perros.mascotahogar.com/Imagenes/raza-husky-siberiano.jpg",
                          ),
                        )),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
