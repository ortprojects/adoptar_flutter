import 'package:adoptar_app/modules/core/presentation/widgets/app_bar_widget.dart';
import 'package:adoptar_app/modules/posts/presentation/widgets/post_list_widget.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  static Route<dynamic> route() => MaterialPageRoute(
        builder: (context) => HomePage(),
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: "Home",
        appBar: AppBar(),
      ),
      body: Container(
        child: PostList(),
      ),
    );
  }
}
