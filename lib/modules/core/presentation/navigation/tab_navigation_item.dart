import 'package:adoptar_app/modules/core/presentation/pages/home_page.dart';
import 'package:adoptar_app/modules/users/presentation/pages/profile_page.dart';
import 'package:adoptar_app/modules/posts/presentation/pages/main_create_post_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class TabNavigationItem {
  final Widget page;
  final String title;
  final IconData icon;

  TabNavigationItem({
    @required this.page,
    @required this.title,
    @required this.icon,
  });

  static List<TabNavigationItem> get items => [
        TabNavigationItem(
          page: HomePage(),
          icon: Icons.home,
          title: "Home",
        ),
        TabNavigationItem(
          page: MainCreatePost(),
          icon: Icons.add,
          title: "Add",
        ),
        TabNavigationItem(
          page: ProfilePage(),
          title: "Profile",
          icon: Icons.person,
        )
      ];
}
