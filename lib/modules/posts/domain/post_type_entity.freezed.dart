// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'post_type_entity.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$PostTypeEntityTearOff {
  const _$PostTypeEntityTearOff();

// ignore: unused_element
  _PostTypeEntity call({@required String title, @required String path}) {
    return _PostTypeEntity(
      title: title,
      path: path,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $PostTypeEntity = _$PostTypeEntityTearOff();

/// @nodoc
mixin _$PostTypeEntity {
  String get title;
  String get path;

  $PostTypeEntityCopyWith<PostTypeEntity> get copyWith;
}

/// @nodoc
abstract class $PostTypeEntityCopyWith<$Res> {
  factory $PostTypeEntityCopyWith(
          PostTypeEntity value, $Res Function(PostTypeEntity) then) =
      _$PostTypeEntityCopyWithImpl<$Res>;
  $Res call({String title, String path});
}

/// @nodoc
class _$PostTypeEntityCopyWithImpl<$Res>
    implements $PostTypeEntityCopyWith<$Res> {
  _$PostTypeEntityCopyWithImpl(this._value, this._then);

  final PostTypeEntity _value;
  // ignore: unused_field
  final $Res Function(PostTypeEntity) _then;

  @override
  $Res call({
    Object title = freezed,
    Object path = freezed,
  }) {
    return _then(_value.copyWith(
      title: title == freezed ? _value.title : title as String,
      path: path == freezed ? _value.path : path as String,
    ));
  }
}

/// @nodoc
abstract class _$PostTypeEntityCopyWith<$Res>
    implements $PostTypeEntityCopyWith<$Res> {
  factory _$PostTypeEntityCopyWith(
          _PostTypeEntity value, $Res Function(_PostTypeEntity) then) =
      __$PostTypeEntityCopyWithImpl<$Res>;
  @override
  $Res call({String title, String path});
}

/// @nodoc
class __$PostTypeEntityCopyWithImpl<$Res>
    extends _$PostTypeEntityCopyWithImpl<$Res>
    implements _$PostTypeEntityCopyWith<$Res> {
  __$PostTypeEntityCopyWithImpl(
      _PostTypeEntity _value, $Res Function(_PostTypeEntity) _then)
      : super(_value, (v) => _then(v as _PostTypeEntity));

  @override
  _PostTypeEntity get _value => super._value as _PostTypeEntity;

  @override
  $Res call({
    Object title = freezed,
    Object path = freezed,
  }) {
    return _then(_PostTypeEntity(
      title: title == freezed ? _value.title : title as String,
      path: path == freezed ? _value.path : path as String,
    ));
  }
}

/// @nodoc
class _$_PostTypeEntity extends _PostTypeEntity with DiagnosticableTreeMixin {
  const _$_PostTypeEntity({@required this.title, @required this.path})
      : assert(title != null),
        assert(path != null),
        super._();

  @override
  final String title;
  @override
  final String path;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'PostTypeEntity(title: $title, path: $path)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'PostTypeEntity'))
      ..add(DiagnosticsProperty('title', title))
      ..add(DiagnosticsProperty('path', path));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _PostTypeEntity &&
            (identical(other.title, title) ||
                const DeepCollectionEquality().equals(other.title, title)) &&
            (identical(other.path, path) ||
                const DeepCollectionEquality().equals(other.path, path)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(title) ^
      const DeepCollectionEquality().hash(path);

  @override
  _$PostTypeEntityCopyWith<_PostTypeEntity> get copyWith =>
      __$PostTypeEntityCopyWithImpl<_PostTypeEntity>(this, _$identity);
}

abstract class _PostTypeEntity extends PostTypeEntity {
  const _PostTypeEntity._() : super._();
  const factory _PostTypeEntity(
      {@required String title, @required String path}) = _$_PostTypeEntity;

  @override
  String get title;
  @override
  String get path;
  @override
  _$PostTypeEntityCopyWith<_PostTypeEntity> get copyWith;
}
