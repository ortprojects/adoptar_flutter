import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'post_type_entity.freezed.dart';

@freezed
abstract class PostTypeEntity with _$PostTypeEntity {
  const PostTypeEntity._();

  const factory PostTypeEntity({
    @required String title,
    @required String path,
  }) = _PostTypeEntity;
}