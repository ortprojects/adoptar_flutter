import 'package:flutter/foundation.dart';

class Slider {
  final String sliderImageUrl;
  final String sliderHeading;
  final String sliderSubHeading;

  Slider(
      {@required this.sliderImageUrl,
      @required this.sliderHeading,
      @required this.sliderSubHeading});
}

final sliderArrayList = [
  Slider(
      sliderImageUrl: 'assets/slides/slider_1.png',
      sliderHeading: "Constants.SLIDER_HEADING_1",
      sliderSubHeading: "Constants.SLIDER_DESC"),
  Slider(
      sliderImageUrl: 'assets/slides/slider_2.png',
      sliderHeading: "Constants.SLIDER_HEADING_2",
      sliderSubHeading: "Constants.SLIDER_DESC"),
  Slider(
      sliderImageUrl: 'assets/slides/slider_3.png',
      sliderHeading: "Constants.SLIDER_HEADING_3",
      sliderSubHeading: "Constants.SLIDER_DESC"),
];
