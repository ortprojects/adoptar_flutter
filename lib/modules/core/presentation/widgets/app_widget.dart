import 'dart:io';

import 'package:adoptar_app/modules/core/guards/auth_guard.dart';
import 'package:adoptar_app/modules/core/routes/router.gr.dart' as app_router;
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AppWidget extends StatelessWidget {
  const AppWidget({Key key}) : super(key: key);
  final bool isFirstTime = true;

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.dark,
      statusBarBrightness:
          Platform.isAndroid ? Brightness.dark : Brightness.light,
      systemNavigationBarColor: Colors.white,
      systemNavigationBarDividerColor: Colors.grey,
      systemNavigationBarIconBrightness: Brightness.dark,
    ));
    return MaterialApp(
      title: 'Adoptions',
      debugShowCheckedModeBanner: false,
      builder: ExtendedNavigator.builder(
        router: app_router.Router(),
        guards: [AuthGuard()],
        initialRoute: isFirstTime ? app_router.Routes.landing_page : app_router.Routes.app_page
      ),
      // theme: ThemeData.light().copyWith(
      //   primaryColor: Colors.green[800],
      //   accentColor: Colors.blueAccent,
      //   floatingActionButtonTheme: FloatingActionButtonThemeData(
      //     backgroundColor: Colors.blue[900],
      //   ),
      //   inputDecorationTheme: InputDecorationTheme(
      //     border: OutlineInputBorder(
      //       borderRadius: BorderRadius.circular(8),
      //     ),
      //   ),
      // ),
    );
  }
}
