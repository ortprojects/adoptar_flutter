// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// ignore_for_file: public_member_api_docs

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

import '../../posts/domain/post_entity.dart';
import '../../posts/presentation/pages/create_post_page.dart';
import '../../posts/presentation/pages/post_page.dart';
import '../guards/auth_guard.dart';
import '../presentation/pages/app_page.dart';
import '../presentation/pages/landing_page.dart';

class Routes {
  static const String app_page = '/app_page';
  static const String post_page = '/post_page';
  static const String create_post = '/create_post';
  static const String landing_page = '/landing_page';
  static const all = <String>{
    app_page,
    post_page,
    create_post,
    landing_page,
  };
}

class Router extends RouterBase {
  @override
  List<RouteDef> get routes => _routes;
  final _routes = <RouteDef>[
    RouteDef(Routes.app_page, page: AppPage, guards: [AuthGuard]),
    RouteDef(Routes.post_page, page: PostPage, guards: [AuthGuard]),
    RouteDef(Routes.create_post, page: CreatePostPage, guards: [AuthGuard]),
    RouteDef(Routes.landing_page, page: LandingPage, guards: [AuthGuard]),
  ];
  @override
  Map<Type, AutoRouteFactory> get pagesMap => _pagesMap;
  final _pagesMap = <Type, AutoRouteFactory>{
    AppPage: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const AppPage(),
        settings: data,
      );
    },
    PostPage: (data) {
      final args = data.getArgs<PostPageArguments>(nullOk: false);
      return MaterialPageRoute<dynamic>(
        builder: (context) => PostPage(
          key: args.key,
          postEntity: args.postEntity,
        ),
        settings: data,
      );
    },
    CreatePostPage: (data) {
      final args = data.getArgs<CreatePostPageArguments>(nullOk: false);
      return MaterialPageRoute<dynamic>(
        builder: (context) => CreatePostPage(
          key: args.key,
          appbarTitle: args.appbarTitle,
        ),
        settings: data,
      );
    },
    LandingPage: (data) {
      final args = data.getArgs<LandingPageArguments>(
        orElse: () => LandingPageArguments(),
      );
      return MaterialPageRoute<dynamic>(
        builder: (context) => LandingPage(key: args.key),
        settings: data,
      );
    },
  };
}

/// ************************************************************************
/// Navigation helper methods extension
/// *************************************************************************

extension RouterExtendedNavigatorStateX on ExtendedNavigatorState {
  Future<dynamic> pushApp_page() => push<dynamic>(Routes.app_page);

  Future<dynamic> pushPost_page(
          {Key key,
          @required PostEntity postEntity,
          OnNavigationRejected onReject}) =>
      push<dynamic>(
        Routes.post_page,
        arguments: PostPageArguments(key: key, postEntity: postEntity),
        onReject: onReject,
      );

  Future<dynamic> pushCreate_post(
          {Key key,
          @required String appbarTitle,
          OnNavigationRejected onReject}) =>
      push<dynamic>(
        Routes.create_post,
        arguments: CreatePostPageArguments(key: key, appbarTitle: appbarTitle),
        onReject: onReject,
      );

  Future<dynamic> pushLanding_page({Key key, OnNavigationRejected onReject}) =>
      push<dynamic>(
        Routes.landing_page,
        arguments: LandingPageArguments(key: key),
        onReject: onReject,
      );
}

/// ************************************************************************
/// Arguments holder classes
/// *************************************************************************

/// PostPage arguments holder class
class PostPageArguments {
  final Key key;
  final PostEntity postEntity;
  PostPageArguments({this.key, @required this.postEntity});
}

/// CreatePostPage arguments holder class
class CreatePostPageArguments {
  final Key key;
  final String appbarTitle;
  CreatePostPageArguments({this.key, @required this.appbarTitle});
}

/// LandingPage arguments holder class
class LandingPageArguments {
  final Key key;
  LandingPageArguments({this.key});
}
