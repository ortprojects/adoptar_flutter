import 'package:adoptar_app/modules/core/guards/auth_guard.dart';
import 'package:adoptar_app/modules/core/presentation/pages/app_page.dart';
import 'package:adoptar_app/modules/core/presentation/pages/landing_page.dart';
import 'package:adoptar_app/modules/posts/presentation/pages/create_post_page.dart';
import 'package:adoptar_app/modules/posts/presentation/pages/post_page.dart';
import 'package:auto_route/auto_route_annotations.dart';

@MaterialAutoRouter(
  generateNavigationHelperExtension: true,
  routes: <AutoRoute>[
    MaterialRoute(
      page: AppPage,
      guards: [AuthGuard],
      name: "app_page",
      path: "/app_page"
    ),
    MaterialRoute(
      page: PostPage,
      name: "post_page",
      path: "/post_page",
      guards: [AuthGuard],
    ),
    MaterialRoute(
      page: CreatePostPage,
      guards: [AuthGuard],
      path: "/create_post",
      name: "create_post",
    ),
    MaterialRoute(
      page: LandingPage,
      guards: [AuthGuard],
      name: "landing_page",
      path: "/landing_page",
    ),
  ],
)
class $Router {}
