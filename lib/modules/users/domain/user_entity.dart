import 'package:adoptar_app/modules/core/domain/unique_id.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'user_entity.freezed.dart';

@freezed
abstract class UserEntity with _$UserEntity {
  const UserEntity._();

  const factory UserEntity({
    @required UniqueId id,
    @required String displayName,
    @required String photoURL,
  }) = _UserEntity;
}
